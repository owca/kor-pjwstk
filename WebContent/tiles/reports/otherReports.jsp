<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<h2>Różne raporty</h2>
<table class="table-bordered">
	<tr>
		<td>Czy wszyscy zarabiają minimalną krajową</td>
		<td>${allHaveMinimumWage}</td>
	</tr>
	<tr>
		<td>Czy jakiś klient ma "ładne imię"</td>
		<td>${hasAnyoneNiceName}</td>
	</tr>
	<tr>
		<td>Czy wśród pracowników jest milioner?</td>
		<td>${isThereAMillionaire}</td>
	</tr>
</table>