<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<h2>Raport o zamówieniach klientów</h2>
<table>
	<tr>
		<th>Klient</th>
		<th>Ilość zamówień</th>
	</tr>
	<c:forEach var="customerWithOrder" items="${customersWithOrders}">
		<tr>
			<td>${customerWithOrder.c.firstName} ${customerWithOrder.c.lastName}</td>
			<td>${customerWithOrder.numberOfOrders}</td>
		</tr>
	</c:forEach>
</table>