<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<h2>Raport płac</h2>
<table class="table-bordered">
	<tr>
		<td>Najniższa pensja</td>
		<td>${minSalary}</td>
	</tr>
	<tr>
		<td>Średnia pensja</td>
		<td>${averageSalary}</td>
	</tr>
	<tr>
		<td>Najwyższa pensja</td>
		<td>${maxSalary}</td>
	</tr>
	<tr>
		<td>Ilość pracowników</td>
		<td>${numberOfEmployees}</td>
	</tr>
	<tr>
		<td>Suma pensji</td>
		<td>${totalSalary}</td>
	</tr>
</table>