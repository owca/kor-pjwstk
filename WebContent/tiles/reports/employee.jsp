<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<h2>Raport o pracowniku ${employee.firstName} ${employee.lastName}</h2>
<form action="/kor/pokazPracownika" method="get">
	<table class="table-bordered">
		<tr>
			<td><select name="employeeId">
					<option>-</option>
					<c:forEach var="employee" items="${employees}">
						<option value="${employee.id}">${employee.firstName}
							${employee.lastName}</option>
					</c:forEach>
			</select></td>
			<td><input type="submit" value="Wyświetl" /></td>
		</tr>
		<tr>
			<td>Pensja</td>
			<td>${employee.salary}</td>
		</tr>
		<tr>
			<td>Pensja roczna</td>
			<td>${yearSalary}</td>
		</tr>
		<tr>
			<td>Pensja brutto (podatek)</td>
			<td>${grossSalary} ${tax * 100}%</td>
		</tr>
		<tr>
		<td colspan="2">
		<strong>Ile trzeba zbierać na Lamborghini</strong><br />
			Cena lamborghini: ${priceOfLamborghini}<br />
			Stopa procentowa: ${interestRate}<br />
			<ul>
				<c:forEach var="month" items="${monthsToGetLamborghini}" varStatus="status">
					${status.count}. ${month}<br />
				</c:forEach>
			</ul>
		</td>
		</tr>
	</table>
</form>