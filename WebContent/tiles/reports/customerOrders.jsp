<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<h2>Raport o zamówieniach klienta</h2>

<form method="get" action="customerOrders">
	<table>
		<tr>
			<td>Klient</td>
			<td><select name="customerLogin">
			<c:forEach var="customer" items="${customers}">
					<option value="${customer.login}">${customer.firstName}
						${customer.lastName}</option>
				</c:forEach> </select></td>
		</tr>
		<tr>
			<td>Od</td>
			<td><input name="dateAfter" id="dateAfter" type="date" /></td>
		</tr>
		<tr>
			<td>Do</td>
			<td><input name="dateBefore" id="dateBefore" type="date" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Pokaż zamówienia" /></td>
		</tr>
	</table>
</form>

<table>
	<tr>
		<th>Data stworzenia</th>
		<th>Data realizacji</th>
	</tr>
	<c:forEach var="order" items="${orders}">
		<tr>
			<td>${order.creationDate}</td>
			<td>${order.executionDate}</td>
		</tr>
	</c:forEach>
</table>