<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function setToRemove(id) {
		$("#toRemove").val(id);
	}
</script>
<form action="/kor/usunZamowienie" method="post">
	<h2>Lista pracowników</h2>
	<table class="table-bordered" width="1000px">
		<tr>
			<th>Klient</th>
			<th>Produkt</th>
			<th>Pracownik</th>
			<th>Data utworzenia zamówienia</th>
			<th>Data realizacji zamówienia</th>
			<th></th>
		</tr>
		<c:forEach var="order" items="${orders}">
			<tr>
				<td>${order.customer.firstName}</td>
				<td>
					<ul>
						<c:forEach var="product" items="${order.products}">
							<li>${product.name}</li>
						</c:forEach>
					</ul>
				</td>
				<td>${order.employee.firstName}${order.employee.lastName}</td>
				<td>${order.creationDate}</td>
				<td>${order.executionDate}</td>
				<td><input type="submit" value="Usuń"
					onclick="setToRemove('${order.id}')" /></td>
			</tr>
		</c:forEach>
	</table>
	<input type="hidden" name="toRemove" id="toRemove" />
</form>
<form action="/kor/dodajZamowienie" method="post">
	<h2>Dodaj pracownika</h2>
	<table class="table-bordered" width="1000px">
		<tr>
			<th>Klient</th>
			<th>Produkt</th>
			<th>Pracownik</th>
			<th>Data utworzenia zamówienia</th>
			<th>Data realizacji zamówienia</th>
			<th></th>
		</tr>
		<tr>
			<td><select name="customerId">
					<option>-</option>
					<c:forEach var="customer" items="${customers}">
						<option value="${customer.id}">${customer.firstName}
							${customer.lastName}</option>
					</c:forEach>
			</select></td>
			<td><select name="productId">
					<option>-</option>
					<c:forEach var="product" items="${products}">
						<option value="${product.id}">${product.name}</option>
					</c:forEach>
			</select></td>
			<td><select name="employeeId">
					<option>-</option>
					<c:forEach var="employee" items="${employees}">
						<option value="${employee.id}">${employee.firstName}
							${employee.lastName}</option>
					</c:forEach>
			</select></td>
			<td><input name="creationDateDate" id="creationDateDate"
				type="date" /><input name="creationDateTime" id="creationDateTime"
				type="time" /></td>
			<td><input name="executionDateDate" id="executionDateDate"
				type="date" /><input name="executionDateTime"
				id="executionDateTime" type="time" /></td>
			<td><input type="submit" name="addNew" value="Dodaj" /></td>
		</tr>
	</table>
</form>

<script>
	function getNowDate() {
		var now = new Date();
		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = now.getFullYear() + "-" + (month) + "-" + (day);

		return today;
	}
	function getNowTime() {
		var now = new Date();
		var hour = ("0" + now.getHours()).slice(-2);
		var minute = ("0" + (now.getMinutes())).slice(-2);

		return hour + ":" + minute;
	}
	$(document).ready(function() {
		$("#creationDateDate").val(getNowDate());
		$("#creationDateTime").val(getNowTime());
		$("#executionDateDate").val(getNowDate());
		$("#executionDateTime").val(getNowTime());
	})
</script>