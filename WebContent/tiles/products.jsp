<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function setProductToRemove(id) {
		$("#toRemove").val(id);
	}
</script>
<form action="/kor/usunProdukt" method="post">
	<h2>Lista produktów</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Nazwa</th>
			<th>Cena</th>
			<th>Kategoria</th>
			<th></th>
		</tr>
		<c:forEach var="product" items="${products}">
			<tr>
				<td>${product.name}</td>
				<td>${product.price}</td>
				<td>${product.category.name}</td>
				<td><input type="submit" value="Usuń"
					onclick="setProductToRemove('${product.name}')" /></td>
			</tr>
		</c:forEach>
	</table>
	<input type="hidden" name="toRemove" id="toRemove" />
</form>
<form action="/kor/dodajProdukt" method="post">
	<h2>Dodaj produkt</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Nazwa</th>
			<th>Cena</th>
			<th>Kategoria</th>
			<th></th>
		</tr>
		<tr>
			<td><input name="name" type="text" /></td>
			<td><input name="price" type="text" /></td>
			<td><select name="category">
					<option>-</option>
					<c:forEach var="category" items="${categories}">
						<option value="${category.name}">${category.name}</option>
					</c:forEach>
			</select></td>
			<td><input type="submit" name="addNew" value="Dodaj" /></td>
		</tr>
	</table>
</form>