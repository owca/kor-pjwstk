<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function setCategoryToRemove(id) {
		$("#toRemove").val(id);
	}
</script>
<link href="static/css/tree.css" rel="stylesheet">
<script src="static/js/tree.js"></script>
<form action="/kor/usunKategorie" method="post">
	<h2>Lista kategorii</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Nazwa</th>
			<th>Podkategorie</th>
			<th></th>
		</tr>
		<c:forEach var="category" items="${categories}">
			<tr>
				<td>${category.name}</td>
				<td>
					<ul>
						<c:forEach var="subcategory" items="${category.subcategories}">
							<li>${subcategory.name}</li>
						</c:forEach>
					</ul>
				</td>
				<td><input type="submit" value="Usuń"
					onclick="setCategoryToRemove('${category.name}')" /></td>
			</tr>
		</c:forEach>
	</table>
	<input type="hidden" name="toRemove" id="toRemove" />
</form>
<form action="/kor/dodajKategorie" method="post">
	<h2>Dodaj Kategorię</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Nazwa</th>
			<th>Nadkategoria</th>
			<th></th>
		</tr>
		<tr>
			<td><input name="name" id="name" type="text" /></td>
			<td><select name="parent">
					<option>-</option>
					<c:forEach var="parent" items="${categories}">
						<option value="${parent.name}">${parent.name}</option>
					</c:forEach>
			</select></td>
			<td><input type="submit" name="addNew" value="Dodaj" /></td>
		</tr>
	</table>

</form>