<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function setCustomerToRemove(id) {
		$("#toRemove").val(id);
	}
</script>
<form action="/kor/usunPracownika" method="post">
	<h2>Lista pracowników</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Pensja</th>
			<th>Przełożony</th>
			<th></th>
		</tr>
		<c:forEach var="employee" items="${employees}">
			<tr>
				<td>${employee.firstName}</td>
				<td>${employee.lastName}</td>
				<td>${employee.salary}</td>
				<td><c:if test="${employee.supervisor != null}">${employee.supervisor.firstName} ${employee.supervisor.lastName}</c:if></td>
				<td><input type="submit" value="Usuń"
					onclick="setCustomerToRemove('${employee.id}')" /></td>
			</tr>
		</c:forEach>
	</table>
	<input type="hidden" name="toRemove" id="toRemove" />
</form>
<form action="/kor/dodajPracownika" method="post">
	<h2>Dodaj pracownika</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Pensja</th>
			<th>Przełożony</th>
			<th></th>
		</tr>
		<tr>
			<td><input name="firstName" type="text" /></td>
			<td><input name="lastName" type="text" /></td>
			<td><input name="salary" type="text" /></td>
			<td><select name="supervisor">
					<option>-</option>
					<c:forEach var="supervisor" items="${supervisors}">
						<option value="${supervisor.id}">${supervisor.firstName}
							${supervisor.lastName}</option>
					</c:forEach>
			</select></td>
			<td><input type="submit" name="addNew" value="Dodaj" /></td>
		</tr>
	</table>
</form>