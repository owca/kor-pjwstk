<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	function setCustomerToRemove(id) {
		$("#toRemove").val(id);
	}
</script>
<form action="/kor/usunKlienta" method="post">
	<h2>Lista klientów</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Login</th>
			<th></th>
		</tr>
		<c:forEach var="customer" items="${customers}">
			<tr>
				<td>${customer.firstName}</td>
				<td>${customer.lastName}</td>
				<td>${customer.login}</td>
				<td><input type="submit" value="Usuń"
					onclick="setCustomerToRemove('${customer.id}')" /></td>
			</tr>
		</c:forEach>
	</table>
	<input type="hidden" name="toRemove" id="toRemove" />
</form>
<form action="/kor/dodajKlienta" method="post">
	<h2>Dodaj klienta</h2>
	<table class="table-bordered" width="600px">
		<tr>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Login</th>
			<th></th>
		</tr>
		<tr>
			<td><input name="firstName" type="text" /></td>
			<td><input name="lastName" type="text" /></td>
			<td><input name="login" type="text" /></td>
			<td><input type="submit" name="addNew" value="Dodaj" /></td>
		</tr>
	</table>
</form>