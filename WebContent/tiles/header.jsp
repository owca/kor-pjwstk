<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/kor">Firma</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">

				<li><a href='<c:url value="klienci" />'>Klienci</a></li>
				<li><a href='<c:url value="pracownicy" />'>Pracownicy</a></li>
				<li><a href='<c:url value="kategorie" />'>Kategorie</a></li>
				<li><a href='<c:url value="produkty" />'>Produkty</a></li>
				<li><a href='<c:url value="zamowienia" />'>Zamówienia</a></li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Raporty <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href='<c:url value="/zarobki" />'>Raport o zarobkach</a></li>
						<li><a href='<c:url value="/pokazPracownika" />'>Informacje o pracowniku</a></li>
						<li><a href='<c:url value="/customersReports" />'>Raporty o klientach</a></li>
						<li><a href='<c:url value="/customerOrders" />'>Zamówienia klienta</a></li>
						<li><a href='<c:url value="/otherReports" />'>Inne raporty</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>