<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" /></title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<script type='text/javascript' src='static/js/jquery-2.0.1.js'></script>
<script src="static/js/bootstrap.js"></script>

</head>
<body>

	<div>
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
	</div>
	<div style="padding: 10px">
		<tiles:insertAttribute name="content"></tiles:insertAttribute>
	</div>
</body>
</html>