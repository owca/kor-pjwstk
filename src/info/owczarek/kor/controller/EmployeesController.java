package info.owczarek.kor.controller;

import info.owczarek.kor.model.Employee;
import info.owczarek.kor.services.EmployeeDao;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EmployeesController {
	private static Logger logger = Logger.getLogger(EmployeesController.class);

	@Autowired
	private EmployeeDao employeeDao;
	
	@RequestMapping("/pracownicy")
	public String showEmployees(Model model) {
		addAttributes(model);

		return "employees";
	}

	@RequestMapping("/dodajPracownika")
	public String addEmployee(Model model, String firstName, String lastName, String salary, String supervisor) {
		Employee supervisorObject = employeeDao.getEmployee(supervisor);
		Employee employee = new Employee(firstName, lastName, Double.parseDouble(salary), supervisorObject);
		logger.info("Adding employee " + employee);
		employeeDao.addEmployee(employee);

		addAttributes(model);

		return "employees";
	}

	@RequestMapping("/usunPracownika")
	public String removeEmployee(Model model, String toRemove) {
		logger.info("Removing employee " + toRemove);

		employeeDao.remove(toRemove);

		addAttributes(model);
		
		return "employees";
	}
	
	private void addAttributes(Model model) {
		List<Employee> employees = employeeDao.getAllEmployees();
		model.addAttribute("employees", employees);
		model.addAttribute("supervisors", employees);
	}
}
