package info.owczarek.kor.controller;

import info.owczarek.kor.model.*;
import info.owczarek.kor.services.*;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductsController {
	private static Logger logger = Logger.getLogger(ProductsController.class);

	@Autowired
	private ProductDao productsDao;
	@Autowired
	private CategoryDao categoryDao;

	@RequestMapping("/produkty")
	public String showProducts(Model model) {

		addAttributes(model);

		return "products";
	}

	@RequestMapping("/dodajProdukt")
	public String addProduct(Model model, String name, String price, String category) {
		Product product = new Product(name, Double.parseDouble(price));
		Category productCategory = categoryDao.getCategory(category);
		productCategory.addProduct(product);
		
		logger.info("Adding product " + name);
		productsDao.addProduct(product);

		addAttributes(model);

		return "products";
	}

	@RequestMapping("/usunProdukt")
	public String removeProduct(Model model, String toRemove) {
		logger.info("Removing product " + toRemove);

		productsDao.remove(toRemove);

		addAttributes(model);

		return "products";
	}

	private void addAttributes(Model model) {
		List<Product> products = productsDao.getAllProducts();
		List<Category> categories = categoryDao.getAllCategories();
		model.addAttribute("products", products);
		model.addAttribute("categories", categories);
	}
}
