package info.owczarek.kor.controller;

import info.owczarek.kor.model.Customer;
import info.owczarek.kor.services.CustomerDao;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CustomersController {
	private static Logger logger = Logger.getLogger(CustomersController.class);

	@Autowired
	private CustomerDao customerDao;
	
	@RequestMapping("/klienci")
	public String showCustomers(Model model) {
		addAttributes(model);

		return "customers";
	}

	@RequestMapping("/dodajKlienta")
	public String addCustomer(Model model, String firstName, String lastName, String login) {
		Customer customer = new Customer(firstName, lastName, login, null);
		logger.info("Adding customer " + customer);
		customerDao.addCustomer(customer);

		addAttributes(model);

		return "customers";
	}

	@RequestMapping("/usunKlienta")
	public String removeClient(Model model, String toRemove) {
		logger.info("Removing customer " + toRemove);

		customerDao.remove(toRemove);

		addAttributes(model);
		
		return "customers";
	}
	
	private void addAttributes(Model model) {
		List<Customer> customers = customerDao.getAllCustomers();
		model.addAttribute("customers", customers);
	}
}
