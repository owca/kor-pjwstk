package info.owczarek.kor.controller;

import info.owczarek.kor.model.*;
import info.owczarek.kor.services.*;

import java.text.*;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrderController {
	private static Logger logger = Logger.getLogger(OrderController.class);
	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm");

	@Autowired
	private ProductDao productDao;
	@Autowired
	private CategoryDao categoryDao;
	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private OrderDao orderDao;

	@RequestMapping("/zamowienia")
	public String showProducts(Model model) {
		addAttributes(model);

		return "orders";
	}

	@RequestMapping("/dodajZamowienie")
	public String addProduct(Model model, String customerId, String productId,
			String employeeId, String creationDateDate,
			String creationDateTime, String executionDateDate,
			String executionDateTime) throws ParseException {
		Order order = new Order();
		Customer customer = customerDao.getCustomer(customerId);
		Product product = productDao.getProduct(productId);
		Employee employee = employeeDao.getEmployee(employeeId);
		order.setCustomer(customer);
		List<Product> products = new ArrayList<>();
		products.add(product);
		order.setProducts(products);
		order.setEmployee(employee);

		String creationDateString = creationDateDate + " " + creationDateTime;
		String executionDateString = executionDateDate + " "
				+ executionDateTime;
		Date creationDate = dateFormat.parse(creationDateString);
		Date executionDate = null;
		if (StringUtils.isNotBlank(executionDateString)) {
			executionDate = dateFormat.parse(executionDateString);
		}

		order.setCreationDate(creationDate);
		order.setExecutionDate(executionDate);

		logger.info("Adding order " + order);
		orderDao.addOrder(order);

		addAttributes(model);

		return "orders";
	}

	@RequestMapping("/usunZamowienie")
	public String removeProduct(Model model, String toRemove) {
		logger.info("Removing order " + toRemove);

		orderDao.remove(toRemove);

		addAttributes(model);

		return "orders";
	}

	private void addAttributes(Model model) {
		List<Order> orders = orderDao.getAllOrders();
		List<Employee> employees = employeeDao.getAllEmployees();
		List<Customer> customers = customerDao.getAllCustomers();
		List<Product> products = productDao.getAllProducts();
		model.addAttribute("orders", orders);
		model.addAttribute("employees", employees);
		model.addAttribute("customers", customers);
		model.addAttribute("products", products);
	}
}
