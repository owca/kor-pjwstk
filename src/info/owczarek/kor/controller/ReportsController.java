package info.owczarek.kor.controller;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;

import info.owczarek.kor.model.*;
import info.owczarek.kor.reports.Reports;
import info.owczarek.kor.services.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ReportsController {
	private static Logger logger = Logger.getLogger(ReportsController.class);
	@Autowired
	private Reports reports;
	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private CustomerDao customerDao;
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@RequestMapping("/zarobki")
	public String showSalaryReport(Model model) {
		model.addAttribute("minSalary", reports.getMinSalary());
		model.addAttribute("averageSalary", reports.getAverageSalary());
		model.addAttribute("maxSalary", reports.getMaxSalary());
		model.addAttribute("totalSalary", reports.getSalaryOutgoings());
		model.addAttribute("numberOfEmployees", reports.getNumberOfEmployees());

		return "salaries";
	}

	@RequestMapping("/pokazPracownika")
	public String showEmployee(Model model, String employeeId) {
		logger.info("Showing employee with id=" + employeeId);
		if (employeeId != null && !"-".equals(employeeId)) {
			Employee employee = reports.getEmployee(employeeId);
			double yearSalary = reports.getYearSalary(employeeId);
			double tax = 0.2;
			double grossSalary = reports.getGrossSalary(employeeId, tax);
			double priceOfLamborghini = 1000000.0;
			double interestRate = 0.3;
			Collection<Double> monthsToGetLamborghini = reports
					.getMonthsToGetLamborghini(employee.getSalary(),
							interestRate, priceOfLamborghini);

			model.addAttribute("employee", employee);
			model.addAttribute("yearSalary", yearSalary);
			model.addAttribute("grossSalary", grossSalary);
			model.addAttribute("tax", tax);
			model.addAttribute("interestRate", interestRate);
			model.addAttribute("priceOfLamborghini", priceOfLamborghini);
			model.addAttribute("monthsToGetLamborghini", monthsToGetLamborghini);
		}

		model.addAttribute("employees", employeeDao.getAllEmployees());

		return "employee";
	}

	@RequestMapping("/customersReports")
	public String showCustomersReport(Model model) {
		@SuppressWarnings("unchecked")
		Collection<Map<String, Object>> customersWithOrders = (Collection<Map<String, Object>>) reports
				.getCustomersWithNumberOfOrders();
		model.addAttribute("customersWithOrders", customersWithOrders);
		return "customersReports";
	}

	@RequestMapping("/customerOrders")
	public String showCustomerOrdersAfter(Model model, String customerLogin,
			@RequestParam(value="dateAfter", required=false) String dateAfterString,
			@RequestParam(value="dateBefore", required=false) String dateBeforeString) throws ParseException {
		if (customerLogin != null) {
			Date creationDateAfter = dateFormat.parse(dateAfterString);
			Date creationDateBefore = dateFormat.parse(dateBeforeString);
			Collection<Order> orders = (Collection<Order>) reports.getCustomerOrdersBetween(customerLogin, creationDateAfter, creationDateBefore);
			model.addAttribute("orders", orders);
		}
		
		model.addAttribute("customers", customerDao.getAllCustomers());
		return "customerOrders";
	}
	
	@RequestMapping("/otherReports")
	public String shorOtherReports(Model model) {
		boolean allHaveMinimumWage = reports.checkMinimumWage();
		boolean hasAnyoneNiceName = reports.hasAnyCustomerNiceName();
		boolean isThereAMillionaire = reports.isThereAMillionaireAmongEmployees();
		model.addAttribute("allHaveMinimumWage", allHaveMinimumWage);
		model.addAttribute("hasAnyoneNiceName", hasAnyoneNiceName);
		model.addAttribute("isThereAMillionaire", isThereAMillionaire);
		return "otherReports";
	}
	
}
