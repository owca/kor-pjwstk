package info.owczarek.kor.controller;

import info.owczarek.kor.model.Category;
import info.owczarek.kor.services.*;
import info.owczarek.kor.services.ProductDao;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController {
	private static Logger logger = Logger.getLogger(CategoryController.class);

	@Autowired
	private ProductDao productsDao;
	@Autowired
	private CategoryDao categoryDao;

	@RequestMapping("/kategorie")
	public String showCategories(Model model) {

		addAttributes(model);

		return "categories";
	}

	@RequestMapping("/dodajKategorie")
	public String addCategory(Model model, String name, String parent) {
		logger.info("Adding category " + name + " parent=" + parent);
		Category newCategory = new Category(name);
		if (parent != null && !"-".equals(parent)) {
			Category parentCategory = categoryDao.getCategory(parent);
			parentCategory.addSubcategory(newCategory);
		}
		
		logger.info("Adding category " + newCategory);
		categoryDao.addCategory(newCategory);
		
		addAttributes(model);

		return "categories";
	}

	@RequestMapping("/usunKategorie")
	public String removeCategory(Model model, String toRemove) {
		logger.info("Removing category " + toRemove);

		categoryDao.remove(toRemove);

		addAttributes(model);

		return "categories";
	}

	private void addAttributes(Model model) {
		List<Category> categories = categoryDao.getAllCategories();
		model.addAttribute("categories", categories);
	}
}
