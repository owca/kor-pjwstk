package info.owczarek.kor.model;

import java.util.UUID;

public class Product {
	private String id;
	private String name;
	private double price;
	private Category category;
	
	public Product() {
		this.id = UUID.randomUUID().toString();
	}

	public Product(String name, double price) {
		this();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
}
