package info.owczarek.kor.model;

public class Customer extends Person {
	private String login;
	private String password;
	
	public Customer() {
		
	}
	
	public Customer(String firstName, String lastName, String login,
			String password) {
		super(firstName, lastName);
		this.login = login;
		this.password = password;
	}

	public Customer(String login) {
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Customer [login=" + login + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", toString()=" + super.toString() + "]";
	}

}
