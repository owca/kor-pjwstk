package info.owczarek.kor.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Order {
	private String id;
	private Employee employee;
	private Customer customer;
	private List<Product> products;
	private Date creationDate;
	private Date executionDate;
	
	public Order() {
		this.id = UUID.randomUUID().toString();
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", employee=" + employee + ", customer="
				+ customer + ", products=" + products + ", creationDate="
				+ creationDate + ", executionDate=" + executionDate + "]";
	}
	
	
}
