package info.owczarek.kor.model;

import java.util.ArrayList;
import java.util.List;

public class Category {
	private String name;
	private List<Product> products;
	private List<Category> subcategories;

	public Category(String name) {
		super();
		this.name = name;
		this.products = new ArrayList();
		this.subcategories = new ArrayList();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Category> getSubcategories() {
		return subcategories;
	}

	public void setSubcategories(List<Category> subcategories) {
		this.subcategories = subcategories;
	}

	public void addSubcategory(Category subcategory) {
		if (!subcategories.contains(subcategory)) {
			subcategories.add(subcategory);
		}
	}

	public void addProduct(Product product) {
		if (!products.contains(product)) {
			products.add(product);
		}
		
		product.setCategory(this);
	}

	@Override
	public String toString() {
		return "Category [name=" + name + ", products=" + products
				+ ", subcategories=" + subcategories + "]";
	}
}
