package info.owczarek.kor.model;

public class Employee extends Person {
	private double salary;
	private Employee supervisor;
	
	public Employee() {
		
	}
	
	public Employee(String firstName, String lastName, double salary,
			Employee supervisor) {
		super(firstName, lastName);
		this.salary = salary;
		this.supervisor = supervisor;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public String toString() {
		return "Employee [salary=" + salary + ", supervisor=" + supervisor
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", id=" + id + "]";
	}
}
