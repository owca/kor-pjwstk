package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery12Db4o0 implements Db4oSBQLQuery<java.util.List<java.lang.Double>> {
    private java.lang.Double salary;
    private java.lang.Double interestRate;
    private java.lang.Double priceOfLamborghini;

    public Reports_SbqlQuery12Db4o0(java.lang.Double salary,
        java.lang.Double interestRate, java.lang.Double priceOfLamborghini) {
        this.salary = salary;
        this.interestRate = interestRate;
        this.priceOfLamborghini = priceOfLamborghini;
    }

    /**
     * query='db.(salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash)'
    '
     **/
    public java.util.List<java.lang.Double> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitCloseByExpression - start salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
        //visitAsExpression - start salary as cash
        //visitIdentifierExpression - start salary
        java.lang.Double _ident_salary = salary;

        //visitIdentifierExpression - end salary
        java.lang.Double _asResult_cash = _ident_salary;

        //visitAsExpression - end salary as cash
        java.util.List<java.lang.Double> _closeByResult = new ArrayList<java.lang.Double>();
        _closeByResult.add(_asResult_cash);

        int _i0 = 0;

        while (_i0 < _closeByResult.size()) {
            java.lang.Double _closeByEl = _closeByResult.get(_i0);

            if (_closeByEl != null) {
                ocb.activate(_closeByEl, 1);
            }

            //visitAsExpression - start (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
            //visitWhereExpression - start cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini
            //visitBinaryAExpression - start cash * (1.0 + interestRate) + salary
            //visitBinaryAExpression - start cash * (1.0 + interestRate)
            //visitIdentifierExpression - start cash
            java.lang.Double _ident_cash = _closeByEl;

            //visitIdentifierExpression - end cash
            //visitBinaryAExpression - start 1.0 + interestRate
            //visitLiteralExpression - start 1.0
            //visitLiteralExpression - end 1.0
            //visitIdentifierExpression - start interestRate
            java.lang.Double _ident_interestRate = interestRate;

            //visitIdentifierExpression - end interestRate
            //OperatorPlus - start 1.0 + interestRate
            java.lang.Double _plusResult = 1.0 + _ident_interestRate;

            //OperatorPlus - end 1.0 + interestRate
            //visitBinaryAExpression - end 1.0 + interestRate
            //OperatorMultiply - start cash * (1.0 + interestRate)
            java.lang.Double _multiplyResult = _ident_cash * _plusResult;

            //OperatorMultiply - end cash * (1.0 + interestRate)
            //visitBinaryAExpression - end cash * (1.0 + interestRate)
            //visitIdentifierExpression - start salary
            java.lang.Double _ident_salary1 = salary;

            //visitIdentifierExpression - end salary
            //OperatorPlus - start cash * (1.0 + interestRate) + salary
            java.lang.Double _plusResult1 = _multiplyResult + _ident_salary1;

            //OperatorPlus - end cash * (1.0 + interestRate) + salary
            //visitBinaryAExpression - end cash * (1.0 + interestRate) + salary
            if (_plusResult1 != null) {
                ocb.activate(_plusResult1, 2);
            }

            java.lang.Double _whereEl = _plusResult1;

            //visitBinaryAExpression - start cash <= priceOfLamborghini
            //visitIdentifierExpression - start cash
            java.lang.Double _ident_cash1 = _closeByEl;

            //visitIdentifierExpression - end cash
            //visitIdentifierExpression - start priceOfLamborghini
            java.lang.Double _ident_priceOfLamborghini = priceOfLamborghini;

            //visitIdentifierExpression - end priceOfLamborghini
            //OperatorLessOrEqual - start cash <= priceOfLamborghini
            Boolean _less_or_equalResult = _ident_cash1 <= _ident_priceOfLamborghini;

            //OperatorLessOrEqual - end cash <= priceOfLamborghini
            //visitBinaryAExpression - end cash <= priceOfLamborghini
            java.lang.Double _whereResult = null;

            if (_less_or_equalResult) {
                _whereResult = _plusResult1;
            }

            //visitWhereExpression - end cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini
            java.lang.Double _asResult_cash1 = _whereResult;

            //visitAsExpression - end (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
            if (_asResult_cash1 != null) {
                _closeByResult.add(_asResult_cash1);
            }

            _i0++;
        }

        //visitCloseByExpression - end salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_closeByResult,
            ocb);

        return _closeByResult;

        //evaluateExpression - end salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash
    }
}
