package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery6Db4o0 implements Db4oSBQLQuery<java.util.Collection<info.owczarek.kor.model.Employee>> {
    private java.lang.String employeeId;

    public Reports_SbqlQuery6Db4o0(java.lang.String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * query='db.(Employee where getId() == employeeId)'
    '
     **/
    public java.util.Collection<info.owczarek.kor.model.Employee> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Employee where getId() == employeeId
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitWhereExpression - start Employee where getId() == employeeId
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta9 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids9 = _classMeta9.getIDs(transLocal);

        for (long _id9 : _ids9) {
            LazyObjectReference _ref9 = transLocal.lazyReferenceFor((int) _id9);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref9.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Employee _whereEl : _ident_Employee) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start getId() == employeeId
            //visitMethodExpression - start getId()
            java.lang.String _mth_getIdResult = _whereEl.getId();

            if (_mth_getIdResult != null) {
                ocb.activate(_mth_getIdResult, 1);
            }

            //visitMethodExpression - end getId()
            //visitIdentifierExpression - start employeeId
            java.lang.String _ident_employeeId = employeeId;

            //visitIdentifierExpression - end employeeId
            //OperatorEquals - start getId() == employeeId
            java.lang.Boolean _equalsResult = OperatorUtils.equalsSafe(_mth_getIdResult,
                    _ident_employeeId);

            //OperatorEquals - end getId() == employeeId
            //visitBinaryAExpression - end getId() == employeeId
            if (_equalsResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Employee where getId() == employeeId
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_whereResult, ocb);

        return _whereResult;

        //evaluateExpression - end Employee where getId() == employeeId
    }
}
