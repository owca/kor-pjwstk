package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery9Db4o0 implements Db4oSBQLQuery<java.lang.Double> {
    public Reports_SbqlQuery9Db4o0() {
    }

    /**
     * query='db.( max(Employee.getSalary()))'
    '
     **/
    public java.lang.Double executeQuery(final ObjectContainerBase ocb,
        final Transaction t) {
        //evaluateExpression - start  max(Employee.getSalary())
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitUnaryExpression - start  max(Employee.getSalary())
        //visitDotExpression - start Employee.getSalary()
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta12 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids12 = _classMeta12.getIDs(transLocal);

        for (long _id12 : _ids12) {
            LazyObjectReference _ref12 = transLocal.lazyReferenceFor((int) _id12);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref12.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<java.lang.Double> _dotResult = new java.util.ArrayList<java.lang.Double>();
        int _dotIndex = 0;

        for (info.owczarek.kor.model.Employee _dotEl : _ident_Employee) {
            if (_dotEl == null) {
                continue;
            }

            if (_dotEl != null) {
                ocb.activate(_dotEl, 1);
            }

            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _dotEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            _dotResult.add(_mth_getSalaryResult);
            _dotIndex++;
        }

        //visitDotExpression - end Employee.getSalary()
        //OperatorMax - start  max(Employee.getSalary())
        Number _max0 = null;

        for (Number _maxEl0 : _dotResult) {
            _max0 = MathUtils.max(_max0, _maxEl0);
        }

        java.lang.Double _maxResult = (java.lang.Double) _max0;
        //OperatorMax - end  max(Employee.getSalary())
        //visitUnaryExpression - end  max(Employee.getSalary())
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_maxResult, ocb);

        return _maxResult;

        //evaluateExpression - end  max(Employee.getSalary())
    }
}
