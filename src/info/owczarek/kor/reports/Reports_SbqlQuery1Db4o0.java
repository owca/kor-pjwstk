package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery1Db4o0 implements Db4oSBQLQuery<java.util.Collection<info.owczarek.kor.model.Order>> {
    private java.lang.String customerLogin;
    private java.util.Date creationDateAfter;
    private java.util.Date creationDateBefore;

    public Reports_SbqlQuery1Db4o0(java.lang.String customerLogin,
        java.util.Date creationDateAfter, java.util.Date creationDateBefore) {
        this.customerLogin = customerLogin;
        this.creationDateAfter = creationDateAfter;
        this.creationDateBefore = creationDateBefore;
    }

    /**
     * query='db.((Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore))'
    '
     **/
    public java.util.Collection<info.owczarek.kor.model.Order> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitBinaryAExpression - start (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
        //visitBinaryAExpression - start (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter)
        //visitWhereExpression - start Order where getCustomer().getLogin() == customerLogin
        //visitIdentifierExpression - start Order
        final java.util.Collection<info.owczarek.kor.model.Order> _ident_Order = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        ClassMetadata _classMeta1 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Order");
        long[] _ids1 = _classMeta1.getIDs(transLocal);

        for (long _id1 : _ids1) {
            LazyObjectReference _ref1 = transLocal.lazyReferenceFor((int) _id1);
            _ident_Order.add((info.owczarek.kor.model.Order) _ref1.getObject());
        }

        //visitIdentifierExpression - end Order
        java.util.Collection<info.owczarek.kor.model.Order> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Order _whereEl : _ident_Order) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start getCustomer().getLogin() == customerLogin
            //visitDotExpression - start getCustomer().getLogin()
            //visitMethodExpression - start getCustomer()
            info.owczarek.kor.model.Customer _mth_getCustomerResult = _whereEl.getCustomer();

            if (_mth_getCustomerResult != null) {
                ocb.activate(_mth_getCustomerResult, 1);
            }

            //visitMethodExpression - end getCustomer()
            info.owczarek.kor.model.Customer _dotEl = _mth_getCustomerResult;

            if (_mth_getCustomerResult != null) {
                ocb.activate(_mth_getCustomerResult, 2);
            }

            //visitMethodExpression - start getLogin()
            java.lang.String _mth_getLoginResult = _dotEl.getLogin();

            if (_mth_getLoginResult != null) {
                ocb.activate(_mth_getLoginResult, 1);
            }

            //visitMethodExpression - end getLogin()
            //visitDotExpression - end getCustomer().getLogin()
            //visitIdentifierExpression - start customerLogin
            java.lang.String _ident_customerLogin = customerLogin;

            //visitIdentifierExpression - end customerLogin
            //OperatorEquals - start getCustomer().getLogin() == customerLogin
            java.lang.Boolean _equalsResult = OperatorUtils.equalsSafe(_mth_getLoginResult,
                    _ident_customerLogin);

            //OperatorEquals - end getCustomer().getLogin() == customerLogin
            //visitBinaryAExpression - end getCustomer().getLogin() == customerLogin
            if (_equalsResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Order where getCustomer().getLogin() == customerLogin
        //visitWhereExpression - start Order where getCreationDate() < creationDateAfter
        //visitIdentifierExpression - start Order
        final java.util.Collection<info.owczarek.kor.model.Order> _ident_Order1 = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        ClassMetadata _classMeta2 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Order");
        long[] _ids2 = _classMeta2.getIDs(transLocal);

        for (long _id2 : _ids2) {
            LazyObjectReference _ref2 = transLocal.lazyReferenceFor((int) _id2);
            _ident_Order1.add((info.owczarek.kor.model.Order) _ref2.getObject());
        }

        //visitIdentifierExpression - end Order
        java.util.Collection<info.owczarek.kor.model.Order> _whereResult1 = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        int _whereLoopIndex1 = 0;

        for (info.owczarek.kor.model.Order _whereEl1 : _ident_Order1) {
            if (_whereEl1 == null) {
                continue;
            }

            if (_whereEl1 != null) {
                ocb.activate(_whereEl1, 1);
            }

            //visitBinaryAExpression - start getCreationDate() < creationDateAfter
            //visitMethodExpression - start getCreationDate()
            java.util.Date _mth_getCreationDateResult = _whereEl1.getCreationDate();

            if (_mth_getCreationDateResult != null) {
                ocb.activate(_mth_getCreationDateResult, 1);
            }

            //visitMethodExpression - end getCreationDate()
            //visitIdentifierExpression - start creationDateAfter
            java.util.Date _ident_creationDateAfter = creationDateAfter;

            //visitIdentifierExpression - end creationDateAfter
            //OperatorLess - start getCreationDate() < creationDateAfter
            Boolean _lessResult = _mth_getCreationDateResult.compareTo(_ident_creationDateAfter) < 0;

            //OperatorLess - end getCreationDate() < creationDateAfter
            //visitBinaryAExpression - end getCreationDate() < creationDateAfter
            if (_lessResult) {
                _whereResult1.add(_whereEl1);
            }

            _whereLoopIndex1++;
        }

        //visitWhereExpression - end Order where getCreationDate() < creationDateAfter
        //OperatorExcept - start (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter)
        Collection _minusLeftCol0 = new ArrayList(_whereResult);
        Collection _minusRightCol0 = new ArrayList(_whereResult1);
        java.util.Collection<info.owczarek.kor.model.Order> _exceptResult = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        _exceptResult.addAll(CollectionUtils.subtract(_minusLeftCol0,
                _minusRightCol0));

        //OperatorExcept - end (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter)
        //visitBinaryAExpression - end (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter)
        //visitWhereExpression - start Order where getCreationDate() > creationDateBefore
        //visitIdentifierExpression - start Order
        final java.util.Collection<info.owczarek.kor.model.Order> _ident_Order2 = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        ClassMetadata _classMeta3 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Order");
        long[] _ids3 = _classMeta3.getIDs(transLocal);

        for (long _id3 : _ids3) {
            LazyObjectReference _ref3 = transLocal.lazyReferenceFor((int) _id3);
            _ident_Order2.add((info.owczarek.kor.model.Order) _ref3.getObject());
        }

        //visitIdentifierExpression - end Order
        java.util.Collection<info.owczarek.kor.model.Order> _whereResult2 = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        int _whereLoopIndex2 = 0;

        for (info.owczarek.kor.model.Order _whereEl2 : _ident_Order2) {
            if (_whereEl2 == null) {
                continue;
            }

            if (_whereEl2 != null) {
                ocb.activate(_whereEl2, 1);
            }

            //visitBinaryAExpression - start getCreationDate() > creationDateBefore
            //visitMethodExpression - start getCreationDate()
            java.util.Date _mth_getCreationDateResult1 = _whereEl2.getCreationDate();

            if (_mth_getCreationDateResult1 != null) {
                ocb.activate(_mth_getCreationDateResult1, 1);
            }

            //visitMethodExpression - end getCreationDate()
            //visitIdentifierExpression - start creationDateBefore
            java.util.Date _ident_creationDateBefore = creationDateBefore;

            //visitIdentifierExpression - end creationDateBefore
            //OperatorMore - start getCreationDate() > creationDateBefore
            Boolean _moreResult = (_mth_getCreationDateResult1 == null)
                ? ((_mth_getCreationDateResult1 == null) ? false : false)
                : ((_mth_getCreationDateResult1 == null) ? true
                                                         : (_mth_getCreationDateResult1.compareTo(_ident_creationDateBefore) > 0));

            //OperatorMore - end getCreationDate() > creationDateBefore
            //visitBinaryAExpression - end getCreationDate() > creationDateBefore
            if (_moreResult) {
                _whereResult2.add(_whereEl2);
            }

            _whereLoopIndex2++;
        }

        //visitWhereExpression - end Order where getCreationDate() > creationDateBefore
        //OperatorExcept - start (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
        Collection _minusLeftCol1 = new ArrayList(_exceptResult);
        Collection _minusRightCol1 = new ArrayList(_whereResult2);
        java.util.Collection<info.owczarek.kor.model.Order> _exceptResult1 = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        _exceptResult1.addAll(CollectionUtils.subtract(_minusLeftCol1,
                _minusRightCol1));
        //OperatorExcept - end (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
        //visitBinaryAExpression - end (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_exceptResult1,
            ocb);

        return _exceptResult1;

        //evaluateExpression - end (Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore)
    }
}
