package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery4Db4o0 implements Db4oSBQLQuery<java.util.Collection<java.lang.Double>> {
    private java.lang.String employeeId;

    public Reports_SbqlQuery4Db4o0(java.lang.String employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * query='db.(Employee where getId() == employeeId).(getSalary() * 12.0)'
    '
     **/
    public java.util.Collection<java.lang.Double> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start (Employee where getId() == employeeId).(getSalary() * 12.0)
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitDotExpression - start (Employee where getId() == employeeId).(getSalary() * 12.0)
        //visitWhereExpression - start Employee where getId() == employeeId
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta7 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids7 = _classMeta7.getIDs(transLocal);

        for (long _id7 : _ids7) {
            LazyObjectReference _ref7 = transLocal.lazyReferenceFor((int) _id7);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref7.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Employee _whereEl : _ident_Employee) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start getId() == employeeId
            //visitMethodExpression - start getId()
            java.lang.String _mth_getIdResult = _whereEl.getId();

            if (_mth_getIdResult != null) {
                ocb.activate(_mth_getIdResult, 1);
            }

            //visitMethodExpression - end getId()
            //visitIdentifierExpression - start employeeId
            java.lang.String _ident_employeeId = employeeId;

            //visitIdentifierExpression - end employeeId
            //OperatorEquals - start getId() == employeeId
            java.lang.Boolean _equalsResult = OperatorUtils.equalsSafe(_mth_getIdResult,
                    _ident_employeeId);

            //OperatorEquals - end getId() == employeeId
            //visitBinaryAExpression - end getId() == employeeId
            if (_equalsResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Employee where getId() == employeeId
        java.util.Collection<java.lang.Double> _dotResult = new java.util.ArrayList<java.lang.Double>();
        int _dotIndex = 0;

        for (info.owczarek.kor.model.Employee _dotEl : _whereResult) {
            if (_dotEl == null) {
                continue;
            }

            if (_dotEl != null) {
                ocb.activate(_dotEl, 1);
            }

            //visitBinaryAExpression - start getSalary() * 12.0
            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _dotEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitLiteralExpression - start 12.0
            //visitLiteralExpression - end 12.0
            //OperatorMultiply - start getSalary() * 12.0
            java.lang.Double _multiplyResult = _mth_getSalaryResult * 12.0;

            //OperatorMultiply - end getSalary() * 12.0
            //visitBinaryAExpression - end getSalary() * 12.0
            if (_multiplyResult != null) {
                ocb.activate(_multiplyResult, 1);
            }

            _dotResult.add(_multiplyResult);
            _dotIndex++;
        }

        //visitDotExpression - end (Employee where getId() == employeeId).(getSalary() * 12.0)
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_dotResult, ocb);

        return _dotResult;

        //evaluateExpression - end (Employee where getId() == employeeId).(getSalary() * 12.0)
    }
}
