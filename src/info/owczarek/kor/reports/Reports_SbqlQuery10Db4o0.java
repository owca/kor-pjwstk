package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery10Db4o0 implements Db4oSBQLQuery<java.lang.Integer> {
    public Reports_SbqlQuery10Db4o0() {
    }

    /**
     * query='db.( count(Employee))'
    '
     **/
    public java.lang.Integer executeQuery(final ObjectContainerBase ocb,
        final Transaction t) {
        //evaluateExpression - start  count(Employee)
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitUnaryExpression - start  count(Employee)
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta13 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids13 = _classMeta13.getIDs(transLocal);

        for (long _id13 : _ids13) {
            LazyObjectReference _ref13 = transLocal.lazyReferenceFor((int) _id13);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref13.getObject());
        }

        //visitIdentifierExpression - end Employee
        //OperatorCount - start  count(Employee)
        java.lang.Integer _countResult = _ident_Employee.size();
        //OperatorCount - end  count(Employee)
        //visitUnaryExpression - end  count(Employee)
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_countResult, ocb);

        return _countResult;

        //evaluateExpression - end  count(Employee)
    }
}
