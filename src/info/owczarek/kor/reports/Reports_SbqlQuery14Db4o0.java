package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery14Db4o0 implements Db4oSBQLQuery<java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct>> {
    public Reports_SbqlQuery14Db4o0() {
    }

    /**
     * query='db.(Employee as E join E.getSalary() as salary)'
    '
     **/
    public java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Employee as E join E.getSalary() as salary
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitJoinExpression - start Employee as E join E.getSalary() as salary
        //visitAsExpression - start Employee as E
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta16 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids16 = _classMeta16.getIDs(transLocal);

        for (long _id16 : _ids16) {
            LazyObjectReference _ref16 = transLocal.lazyReferenceFor((int) _id16);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref16.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _asResult_E = _ident_Employee;

        //visitAsExpression - end Employee as E
        java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> _joinResult =
            new java.util.ArrayList<pl.wcislo.sbql4j.java.model.runtime.Struct>();
        int _joinIndex = 0;

        for (info.owczarek.kor.model.Employee _joinEl : _asResult_E) {
            if (_joinEl != null) {
                ocb.activate(_joinEl, 1);
            }

            //visitAsExpression - start E.getSalary() as salary
            //visitDotExpression - start E.getSalary()
            //visitIdentifierExpression - start E
            info.owczarek.kor.model.Employee _ident_E = _joinEl;

            if (_ident_E != null) {
                ocb.activate(_ident_E, 1);
            }

            //visitIdentifierExpression - end E
            info.owczarek.kor.model.Employee _dotEl = _ident_E;

            if (_ident_E != null) {
                ocb.activate(_ident_E, 2);
            }

            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _dotEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitDotExpression - end E.getSalary()
            java.lang.Double _asResult_salary = _mth_getSalaryResult;
            //visitAsExpression - end E.getSalary() as salary
            _joinResult.add(OperatorUtils.cartesianProductSS(_joinEl,
                    _asResult_salary, "E", "salary"));
            _joinIndex++;
        }

        //visitJoinExpression - end Employee as E join E.getSalary() as salary
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_joinResult, ocb);

        return _joinResult;

        //evaluateExpression - end Employee as E join E.getSalary() as salary
    }
}
