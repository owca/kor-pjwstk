package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery18Db4o0 implements Db4oSBQLQuery<java.lang.Boolean> {
    public Reports_SbqlQuery18Db4o0() {
    }

    /**
     * query='db.( exists (Employee where getSalary() > 1000000.0))'
    '
     **/
    public java.lang.Boolean executeQuery(final ObjectContainerBase ocb,
        final Transaction t) {
        //evaluateExpression - start  exists (Employee where getSalary() > 1000000.0)
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitUnaryExpression - start  exists (Employee where getSalary() > 1000000.0)
        //visitWhereExpression - start Employee where getSalary() > 1000000.0
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta20 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids20 = _classMeta20.getIDs(transLocal);

        for (long _id20 : _ids20) {
            LazyObjectReference _ref20 = transLocal.lazyReferenceFor((int) _id20);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref20.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Employee _whereEl : _ident_Employee) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start getSalary() > 1000000.0
            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _whereEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitLiteralExpression - start 1000000.0
            //visitLiteralExpression - end 1000000.0
            //OperatorMore - start getSalary() > 1000000.0
            Boolean _moreResult = (_mth_getSalaryResult == null) ? false
                                                                 : (_mth_getSalaryResult > 1000000.0);

            //OperatorMore - end getSalary() > 1000000.0
            //visitBinaryAExpression - end getSalary() > 1000000.0
            if (_moreResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Employee where getSalary() > 1000000.0
        //OperatorExists - start  exists (Employee where getSalary() > 1000000.0)
        java.lang.Boolean _existsResult = !_whereResult.isEmpty();
        //OperatorExists - end  exists (Employee where getSalary() > 1000000.0)
        //visitUnaryExpression - end  exists (Employee where getSalary() > 1000000.0)
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_existsResult, ocb);

        return _existsResult;

        //evaluateExpression - end  exists (Employee where getSalary() > 1000000.0)
    }
}
