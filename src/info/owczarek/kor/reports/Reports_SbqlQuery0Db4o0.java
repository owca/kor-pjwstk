package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery0Db4o0 implements Db4oSBQLQuery<java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct>> {
    private java.lang.String lastName;

    public Reports_SbqlQuery0Db4o0(java.lang.String lastName) {
        this.lastName = lastName;
    }

    /**
     * query='db.(Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation)'
    '
     **/
    public java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitJoinExpression - start Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
        //visitWhereExpression - start Employee as e where e.getLastName() == lastName
        //visitAsExpression - start Employee as e
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta0 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids0 = _classMeta0.getIDs(transLocal);

        for (long _id0 : _ids0) {
            LazyObjectReference _ref0 = transLocal.lazyReferenceFor((int) _id0);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref0.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _asResult_e = _ident_Employee;

        //visitAsExpression - end Employee as e
        java.util.Collection<info.owczarek.kor.model.Employee> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Employee _whereEl : _asResult_e) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start e.getLastName() == lastName
            //visitDotExpression - start e.getLastName()
            //visitIdentifierExpression - start e
            info.owczarek.kor.model.Employee _ident_e = _whereEl;

            if (_ident_e != null) {
                ocb.activate(_ident_e, 1);
            }

            //visitIdentifierExpression - end e
            info.owczarek.kor.model.Employee _dotEl = _ident_e;

            if (_ident_e != null) {
                ocb.activate(_ident_e, 2);
            }

            //visitMethodExpression - start getLastName()
            java.lang.String _mth_getLastNameResult = _dotEl.getLastName();

            if (_mth_getLastNameResult != null) {
                ocb.activate(_mth_getLastNameResult, 1);
            }

            //visitMethodExpression - end getLastName()
            //visitDotExpression - end e.getLastName()
            //visitIdentifierExpression - start lastName
            java.lang.String _ident_lastName = lastName;

            //visitIdentifierExpression - end lastName
            //OperatorEquals - start e.getLastName() == lastName
            java.lang.Boolean _equalsResult = OperatorUtils.equalsSafe(_mth_getLastNameResult,
                    _ident_lastName);

            //OperatorEquals - end e.getLastName() == lastName
            //visitBinaryAExpression - end e.getLastName() == lastName
            if (_equalsResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Employee as e where e.getLastName() == lastName
        java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> _joinResult =
            new java.util.ArrayList<pl.wcislo.sbql4j.java.model.runtime.Struct>();
        int _joinIndex = 0;

        for (info.owczarek.kor.model.Employee _joinEl : _whereResult) {
            if (_joinEl != null) {
                ocb.activate(_joinEl, 1);
            }

            //visitGroupAsExpression - start ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
            //visitCommaExpression - start (e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary
            //visitAsExpression - start (e.getSalary() > 2000.0) as isSalaryBig
            //visitBinaryAExpression - start e.getSalary() > 2000.0
            //visitDotExpression - start e.getSalary()
            //visitIdentifierExpression - start e
            info.owczarek.kor.model.Employee _ident_e1 = _joinEl;

            if (_ident_e1 != null) {
                ocb.activate(_ident_e1, 1);
            }

            //visitIdentifierExpression - end e
            info.owczarek.kor.model.Employee _dotEl1 = _ident_e1;

            if (_ident_e1 != null) {
                ocb.activate(_ident_e1, 2);
            }

            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _dotEl1.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitDotExpression - end e.getSalary()
            //visitLiteralExpression - start 2000.0
            //visitLiteralExpression - end 2000.0
            //OperatorMore - start e.getSalary() > 2000.0
            Boolean _moreResult = (_mth_getSalaryResult == null) ? false
                                                                 : (_mth_getSalaryResult > 2000.0);

            //OperatorMore - end e.getSalary() > 2000.0
            //visitBinaryAExpression - end e.getSalary() > 2000.0
            java.lang.Boolean _asResult_isSalaryBig = _moreResult;

            //visitAsExpression - end (e.getSalary() > 2000.0) as isSalaryBig
            //visitAsExpression - start e.getSalary() as salary
            //visitDotExpression - start e.getSalary()
            //visitIdentifierExpression - start e
            info.owczarek.kor.model.Employee _ident_e2 = _joinEl;

            if (_ident_e2 != null) {
                ocb.activate(_ident_e2, 1);
            }

            //visitIdentifierExpression - end e
            info.owczarek.kor.model.Employee _dotEl2 = _ident_e2;

            if (_ident_e2 != null) {
                ocb.activate(_ident_e2, 2);
            }

            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult1 = _dotEl2.getSalary();

            if (_mth_getSalaryResult1 != null) {
                ocb.activate(_mth_getSalaryResult1, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitDotExpression - end e.getSalary()
            java.lang.Double _asResult_salary = _mth_getSalaryResult1;

            //visitAsExpression - end e.getSalary() as salary
            //OperatorComma - start (e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary
            pl.wcislo.sbql4j.java.model.runtime.Struct _commaResult = OperatorUtils.cartesianProductSS(_asResult_isSalaryBig,
                    _asResult_salary, "isSalaryBig", "salary");

            //OperatorComma - end (e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary
            //visitCommaExpression - end (e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary
            pl.wcislo.sbql4j.java.model.runtime.Struct _groupAsResultsalaryInformation =
                _commaResult;
            //visitGroupAsExpression - end ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
            _joinResult.add(OperatorUtils.cartesianProductSS(_joinEl,
                    _groupAsResultsalaryInformation, "e", "salaryInformation"));
            _joinIndex++;
        }

        //visitJoinExpression - end Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_joinResult, ocb);

        return _joinResult;

        //evaluateExpression - end Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation
    }
}
