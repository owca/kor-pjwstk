package info.owczarek.kor.reports;

import com.db4o.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.compiletime.Signature.SCollectionType;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery12 {
    private com.db4o.ObjectContainer db;
    private java.lang.Double salary;
    private java.lang.Double interestRate;
    private java.lang.Double priceOfLamborghini;

    public Reports_SbqlQuery12(final com.db4o.ObjectContainer db,
        final double salary, final double interestRate,
        final double priceOfLamborghini) {
        this.db = db;
        this.salary = salary;
        this.interestRate = interestRate;
        this.priceOfLamborghini = priceOfLamborghini;
    }

    /**
     * original query='db.(
                                    salary as cash close by ((cash * (1.0 + interestRate)) + salary where cash <= priceOfLamborghini) as cash
                            )'
     *
     * query after optimization='db.(salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash)'
    */
    public java.util.List<java.lang.Double> executeQuery() {
        //evaluateExpression - start db.(salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash)
        //visitDotExpression - start db.(salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash)
        //visitIdentifierExpression - start db
        com.db4o.ObjectContainer _ident_db = db;

        //visitIdentifierExpression - end db
        java.util.List<java.lang.Double> _queryResult = _ident_db.query(new Reports_SbqlQuery12Db4o0(
                    salary, interestRate, priceOfLamborghini));

        return _queryResult;

        //evaluateExpression - end db.(salary as cash close by (cash * (1.0 + interestRate) + salary where cash <= priceOfLamborghini) as cash)
    }
}
