package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery13Db4o0 implements Db4oSBQLQuery<java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct>> {
    public Reports_SbqlQuery13Db4o0() {
    }

    /**
     * query='db.Employee.(getSalary(), getSupervisor())'
    '
     **/
    public java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Employee.(getSalary(), getSupervisor())
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitDotExpression - start Employee.(getSalary(), getSupervisor())
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta15 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids15 = _classMeta15.getIDs(transLocal);

        for (long _id15 : _ids15) {
            LazyObjectReference _ref15 = transLocal.lazyReferenceFor((int) _id15);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref15.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> _dotResult =
            new java.util.ArrayList<pl.wcislo.sbql4j.java.model.runtime.Struct>();
        int _dotIndex = 0;

        for (info.owczarek.kor.model.Employee _dotEl : _ident_Employee) {
            if (_dotEl == null) {
                continue;
            }

            if (_dotEl != null) {
                ocb.activate(_dotEl, 2);
            }

            //visitCommaExpression - start getSalary(), getSupervisor()
            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _dotEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitMethodExpression - start getSupervisor()
            info.owczarek.kor.model.Employee _mth_getSupervisorResult = _dotEl.getSupervisor();

            if (_mth_getSupervisorResult != null) {
                ocb.activate(_mth_getSupervisorResult, 1);
            }

            //visitMethodExpression - end getSupervisor()
            //OperatorComma - start getSalary(), getSupervisor()
            pl.wcislo.sbql4j.java.model.runtime.Struct _commaResult = OperatorUtils.cartesianProductSS(_mth_getSalaryResult,
                    _mth_getSupervisorResult, "", "");

            //OperatorComma - end getSalary(), getSupervisor()
            //visitCommaExpression - end getSalary(), getSupervisor()
            if (_commaResult != null) {
                ocb.activate(_commaResult, 2);
            }

            _dotResult.add(_commaResult);
            _dotIndex++;
        }

        //visitDotExpression - end Employee.(getSalary(), getSupervisor())
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_dotResult, ocb);

        return _dotResult;

        //evaluateExpression - end Employee.(getSalary(), getSupervisor())
    }
}
