package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery2Db4o0 implements Db4oSBQLQuery<java.util.List<pl.wcislo.sbql4j.java.model.runtime.Struct>> {
    public Reports_SbqlQuery2Db4o0() {
    }

    /**
     * query='db.((Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders) order by (numberOfOrders))'
    '
     **/
    public java.util.List<pl.wcislo.sbql4j.java.model.runtime.Struct> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start (Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders) order by (numberOfOrders)
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitOrderByExpression - start (Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders) order by (numberOfOrders)
        //visitJoinExpression - start Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders
        //visitAsExpression - start Customer as c
        //visitIdentifierExpression - start Customer
        final java.util.Collection<info.owczarek.kor.model.Customer> _ident_Customer =
            new java.util.ArrayList<info.owczarek.kor.model.Customer>();
        ClassMetadata _classMeta4 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Customer");
        long[] _ids4 = _classMeta4.getIDs(transLocal);

        for (long _id4 : _ids4) {
            LazyObjectReference _ref4 = transLocal.lazyReferenceFor((int) _id4);
            _ident_Customer.add((info.owczarek.kor.model.Customer) _ref4.getObject());
        }

        //visitIdentifierExpression - end Customer
        java.util.Collection<info.owczarek.kor.model.Customer> _asResult_c = _ident_Customer;

        //visitAsExpression - end Customer as c
        java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> _joinResult =
            new java.util.ArrayList<pl.wcislo.sbql4j.java.model.runtime.Struct>();
        int _joinIndex = 0;

        for (info.owczarek.kor.model.Customer _joinEl : _asResult_c) {
            if (_joinEl != null) {
                ocb.activate(_joinEl, 1);
            }

            //visitAsExpression - start  count((Order where getCustomer() == c)) as numberOfOrders
            //visitUnaryExpression - start  count((Order where getCustomer() == c))
            //visitWhereExpression - start Order where getCustomer() == c
            //visitIdentifierExpression - start Order
            final java.util.Collection<info.owczarek.kor.model.Order> _ident_Order =
                new java.util.ArrayList<info.owczarek.kor.model.Order>();
            ClassMetadata _classMeta5 = ocb.classCollection()
                                           .getClassMetadata("info.owczarek.kor.model.Order");
            long[] _ids5 = _classMeta5.getIDs(transLocal);

            for (long _id5 : _ids5) {
                LazyObjectReference _ref5 = transLocal.lazyReferenceFor((int) _id5);
                _ident_Order.add((info.owczarek.kor.model.Order) _ref5.getObject());
            }

            //visitIdentifierExpression - end Order
            java.util.Collection<info.owczarek.kor.model.Order> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Order>();
            int _whereLoopIndex = 0;

            for (info.owczarek.kor.model.Order _whereEl : _ident_Order) {
                if (_whereEl == null) {
                    continue;
                }

                if (_whereEl != null) {
                    ocb.activate(_whereEl, 1);
                }

                //visitBinaryAExpression - start getCustomer() == c
                //visitMethodExpression - start getCustomer()
                info.owczarek.kor.model.Customer _mth_getCustomerResult = _whereEl.getCustomer();

                if (_mth_getCustomerResult != null) {
                    ocb.activate(_mth_getCustomerResult, 1);
                }

                //visitMethodExpression - end getCustomer()
                //visitIdentifierExpression - start c
                info.owczarek.kor.model.Customer _ident_c = _joinEl;

                if (_ident_c != null) {
                    ocb.activate(_ident_c, 1);
                }

                //visitIdentifierExpression - end c
                //OperatorEquals - start getCustomer() == c
                java.lang.Boolean _equalsResult = OperatorUtils.equalsSafe(_mth_getCustomerResult,
                        _ident_c);

                //OperatorEquals - end getCustomer() == c
                //visitBinaryAExpression - end getCustomer() == c
                if (_equalsResult) {
                    _whereResult.add(_whereEl);
                }

                _whereLoopIndex++;
            }

            //visitWhereExpression - end Order where getCustomer() == c
            //OperatorCount - start  count((Order where getCustomer() == c))
            java.lang.Integer _countResult = _whereResult.size();

            //OperatorCount - end  count((Order where getCustomer() == c))
            //visitUnaryExpression - end  count((Order where getCustomer() == c))
            java.lang.Integer _asResult_numberOfOrders = _countResult;
            //visitAsExpression - end  count((Order where getCustomer() == c)) as numberOfOrders
            _joinResult.add(OperatorUtils.cartesianProductSS(_joinEl,
                    _asResult_numberOfOrders, "c", "numberOfOrders"));
            _joinIndex++;
        }

        //visitJoinExpression - end Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders
        java.util.List<pl.wcislo.sbql4j.java.model.runtime.Struct> _orderByResult =
            new java.util.ArrayList<pl.wcislo.sbql4j.java.model.runtime.Struct>();

        if (_joinResult != null) {
            ocb.activate(_joinResult, 2);
        }

        _orderByResult.addAll(_joinResult);

        Comparator<pl.wcislo.sbql4j.java.model.runtime.Struct> _comparator0 = new Comparator<pl.wcislo.sbql4j.java.model.runtime.Struct>() {
                public int compare(
                    pl.wcislo.sbql4j.java.model.runtime.Struct _leftObj,
                    pl.wcislo.sbql4j.java.model.runtime.Struct _rightObj) {
                    if (_leftObj == null) {
                        return -1;
                    }

                    if (_leftObj != null) {
                        ocb.activate(_leftObj, 1);
                    }

                    if (_rightObj != null) {
                        ocb.activate(_rightObj, 1);
                    }

                    int res = 0;
                    java.lang.Integer _leftParam0;

                    {
                        //visitOrderByParamExpression - start numberOfOrders
                        //visitIdentifierExpression - start numberOfOrders
                        java.lang.Integer _ident_numberOfOrders = (java.lang.Integer) _leftObj.get(
                                "numberOfOrders");

                        if (_ident_numberOfOrders != null) {
                            ocb.activate(_ident_numberOfOrders, 1);
                        }

                        //visitIdentifierExpression - end numberOfOrders
                        //visitOrderByParamExpression - end numberOfOrders
                        _leftParam0 = _ident_numberOfOrders;
                    }

                    java.lang.Integer _rightParam0;

                    {
                        //visitOrderByParamExpression - start numberOfOrders
                        //visitIdentifierExpression - start numberOfOrders
                        java.lang.Integer _ident_numberOfOrders = (java.lang.Integer) _rightObj.get(
                                "numberOfOrders");

                        if (_ident_numberOfOrders != null) {
                            ocb.activate(_ident_numberOfOrders, 1);
                        }

                        //visitIdentifierExpression - end numberOfOrders
                        //visitOrderByParamExpression - end numberOfOrders
                        _rightParam0 = _ident_numberOfOrders;
                    }

                    if (_leftParam0 != null) {
                        res = _leftParam0.compareTo(_rightParam0);
                    } else {
                        return -1;
                    }

                    return res;
                }
            };

        Collections.sort(_orderByResult, _comparator0);
        //visitOrderByExpression - end (Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders) order by (numberOfOrders)
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_orderByResult,
            ocb);

        return _orderByResult;

        //evaluateExpression - end (Customer as c join  count((Order where getCustomer() == c)) as numberOfOrders) order by (numberOfOrders)
    }
}
