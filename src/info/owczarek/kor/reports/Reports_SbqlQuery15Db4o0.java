package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery15Db4o0 implements Db4oSBQLQuery<java.util.Collection<info.owczarek.kor.model.Employee>> {
    private java.lang.Double richnessLevel;

    public Reports_SbqlQuery15Db4o0(java.lang.Double richnessLevel) {
        this.richnessLevel = richnessLevel;
    }

    /**
     * query='db.(Employee where getSalary() > richnessLevel)'
    '
     **/
    public java.util.Collection<info.owczarek.kor.model.Employee> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Employee where getSalary() > richnessLevel
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitWhereExpression - start Employee where getSalary() > richnessLevel
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta17 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids17 = _classMeta17.getIDs(transLocal);

        for (long _id17 : _ids17) {
            LazyObjectReference _ref17 = transLocal.lazyReferenceFor((int) _id17);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref17.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.util.Collection<info.owczarek.kor.model.Employee> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Employee _whereEl : _ident_Employee) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start getSalary() > richnessLevel
            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _whereEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitIdentifierExpression - start richnessLevel
            java.lang.Double _ident_richnessLevel = richnessLevel;

            //visitIdentifierExpression - end richnessLevel
            //OperatorMore - start getSalary() > richnessLevel
            Boolean _moreResult = (_mth_getSalaryResult == null) ? false
                                                                 : (_mth_getSalaryResult > _ident_richnessLevel);

            //OperatorMore - end getSalary() > richnessLevel
            //visitBinaryAExpression - end getSalary() > richnessLevel
            if (_moreResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Employee where getSalary() > richnessLevel
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_whereResult, ocb);

        return _whereResult;

        //evaluateExpression - end Employee where getSalary() > richnessLevel
    }
}
