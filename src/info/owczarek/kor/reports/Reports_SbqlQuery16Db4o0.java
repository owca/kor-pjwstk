package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery16Db4o0 implements Db4oSBQLQuery<java.lang.Boolean> {
    private java.lang.Double minimumWage;

    public Reports_SbqlQuery16Db4o0(java.lang.Double minimumWage) {
        this.minimumWage = minimumWage;
    }

    /**
     * query='db.( all Employee getSalary() > minimumWage)'
    '
     **/
    public java.lang.Boolean executeQuery(final ObjectContainerBase ocb,
        final Transaction t) {
        //evaluateExpression - start  all Employee getSalary() > minimumWage
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitForallExpression - start  all Employee getSalary() > minimumWage
        //visitIdentifierExpression - start Employee
        final java.util.Collection<info.owczarek.kor.model.Employee> _ident_Employee =
            new java.util.ArrayList<info.owczarek.kor.model.Employee>();
        ClassMetadata _classMeta18 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Employee");
        long[] _ids18 = _classMeta18.getIDs(transLocal);

        for (long _id18 : _ids18) {
            LazyObjectReference _ref18 = transLocal.lazyReferenceFor((int) _id18);
            _ident_Employee.add((info.owczarek.kor.model.Employee) _ref18.getObject());
        }

        //visitIdentifierExpression - end Employee
        java.lang.Boolean _allResult = true;
        Integer _allIndex = 0;

        for (info.owczarek.kor.model.Employee _allEl : _ident_Employee) {
            if (_allEl != null) {
                ocb.activate(_allEl, 1);
            }

            //visitBinaryAExpression - start getSalary() > minimumWage
            //visitMethodExpression - start getSalary()
            java.lang.Double _mth_getSalaryResult = _allEl.getSalary();

            if (_mth_getSalaryResult != null) {
                ocb.activate(_mth_getSalaryResult, 1);
            }

            //visitMethodExpression - end getSalary()
            //visitIdentifierExpression - start minimumWage
            java.lang.Double _ident_minimumWage = minimumWage;

            //visitIdentifierExpression - end minimumWage
            //OperatorMore - start getSalary() > minimumWage
            Boolean _moreResult = (_mth_getSalaryResult == null) ? false
                                                                 : (_mth_getSalaryResult > _ident_minimumWage);

            //OperatorMore - end getSalary() > minimumWage
            //visitBinaryAExpression - end getSalary() > minimumWage
            if (!_moreResult) {
                _allResult = false;

                break;
            }
        }

        //visitForallExpression - end  all Employee getSalary() > minimumWage
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_allResult, ocb);

        return _allResult;

        //evaluateExpression - end  all Employee getSalary() > minimumWage
    }
}
