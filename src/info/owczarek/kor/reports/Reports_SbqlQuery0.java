package info.owczarek.kor.reports;

import com.db4o.*;

import info.owczarek.kor.model.*;
import info.owczarek.kor.model.Employee;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.compiletime.Signature.SCollectionType;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery0 {
    private com.db4o.ObjectContainer db;
    private java.lang.String lastName;

    public Reports_SbqlQuery0(final com.db4o.ObjectContainer db,
        final java.lang.String lastName) {
        this.db = db;
        this.lastName = lastName;
    }

    /**
     * original query='db. (
                                    (Employee as e where e.lastName == lastName) join (
                                                    (((e.salary > 2000.0) as isSalaryBig), (e.salary as salary)
                                                    ) group as salaryInformation)
                                    )'
     *
     * query after optimization='db.(Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation)'
    */
    public java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> executeQuery() {
        //evaluateExpression - start db.(Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation)
        //visitDotExpression - start db.(Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation)
        //visitIdentifierExpression - start db
        com.db4o.ObjectContainer _ident_db = db;

        //visitIdentifierExpression - end db
        java.util.Collection<pl.wcislo.sbql4j.java.model.runtime.Struct> _queryResult =
            _ident_db.query(new Reports_SbqlQuery0Db4o0(lastName));

        return _queryResult;

        //evaluateExpression - end db.(Employee as e where e.getLastName() == lastName join ((e.getSalary() > 2000.0) as isSalaryBig, e.getSalary() as salary) group as salaryInformation)
    }
}
