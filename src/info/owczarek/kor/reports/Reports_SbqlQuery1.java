package info.owczarek.kor.reports;

import com.db4o.*;

import info.owczarek.kor.model.*;
import info.owczarek.kor.model.Order;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.compiletime.Signature.SCollectionType;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery1 {
    private com.db4o.ObjectContainer db;
    private java.lang.String customerLogin;
    private java.util.Date creationDateAfter;
    private java.util.Date creationDateBefore;

    public Reports_SbqlQuery1(final com.db4o.ObjectContainer db,
        final java.lang.String customerLogin,
        final java.util.Date creationDateAfter,
        final java.util.Date creationDateBefore) {
        this.db = db;
        this.customerLogin = customerLogin;
        this.creationDateAfter = creationDateAfter;
        this.creationDateBefore = creationDateBefore;
    }

    /**
     * original query='db. (
                            (Order where (customer.login == customerLogin)) minus (Order where (creationDate < creationDateAfter)) minus (Order where (creationDate > creationDateBefore))
                    )'
     *
     * query after optimization='db.((Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore))'
    */
    public java.util.Collection<info.owczarek.kor.model.Order> executeQuery() {
        //evaluateExpression - start db.((Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore))
        //visitDotExpression - start db.((Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore))
        //visitIdentifierExpression - start db
        com.db4o.ObjectContainer _ident_db = db;

        //visitIdentifierExpression - end db
        java.util.Collection<info.owczarek.kor.model.Order> _queryResult = _ident_db.query(new Reports_SbqlQuery1Db4o0(
                    customerLogin, creationDateAfter, creationDateBefore));

        return _queryResult;

        //evaluateExpression - end db.((Order where getCustomer().getLogin() == customerLogin) minus (Order where getCreationDate() < creationDateAfter) minus (Order where getCreationDate() > creationDateBefore))
    }
}
