package info.owczarek.kor.reports;

import com.db4o.*;

import info.owczarek.kor.model.*;
import info.owczarek.kor.model.Employee;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.compiletime.Signature.SCollectionType;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery5 {
    private java.lang.Double tax;
    private com.db4o.ObjectContainer db;
    private java.lang.String employeeId;

    public Reports_SbqlQuery5(final double tax,
        final com.db4o.ObjectContainer db, final java.lang.String employeeId) {
        this.tax = tax;
        this.db = db;
        this.employeeId = employeeId;
    }

    /**
     * original query='db.(
                            (Employee where id == employeeId).(salary * (1.0 + tax))
                    )'
     *
     * query after optimization='((1.0 + tax) group as _aux0).db.(Employee where getId() == employeeId).(getSalary() * _aux0)'
    */
    public java.util.Collection<java.lang.Double> executeQuery() {
        //evaluateExpression - start ((1.0 + tax) group as _aux0).db.(Employee where getId() == employeeId).(getSalary() * _aux0)
        //visitDotExpression - start ((1.0 + tax) group as _aux0).db.(Employee where getId() == employeeId).(getSalary() * _aux0)
        //visitGroupAsExpression - start (1.0 + tax) group as _aux0
        //visitBinaryAExpression - start 1.0 + tax
        //visitLiteralExpression - start 1.0
        //visitLiteralExpression - end 1.0
        //visitIdentifierExpression - start tax
        java.lang.Double _ident_tax = tax;

        //visitIdentifierExpression - end tax
        //OperatorPlus - start 1.0 + tax
        java.lang.Double _plusResult = 1.0 + _ident_tax;

        //OperatorPlus - end 1.0 + tax
        //visitBinaryAExpression - end 1.0 + tax
        java.lang.Double _groupAsResult_aux0 = _plusResult;

        //visitGroupAsExpression - end (1.0 + tax) group as _aux0
        java.lang.Double _dotEl2 = _groupAsResult_aux0;

        //visitDotExpression - start db.(Employee where getId() == employeeId).(getSalary() * _aux0)
        //visitIdentifierExpression - start db
        com.db4o.ObjectContainer _ident_db = db;

        //visitIdentifierExpression - end db
        java.util.Collection<java.lang.Double> _dotResult1 = _ident_db.query(new Reports_SbqlQuery5Db4o0(
                    employeeId, _groupAsResult_aux0));

        //visitDotExpression - end ((1.0 + tax) group as _aux0).db.(Employee where getId() == employeeId).(getSalary() * _aux0)
        return _dotResult1;

        //evaluateExpression - end ((1.0 + tax) group as _aux0).db.(Employee where getId() == employeeId).(getSalary() * _aux0)
    }
}
