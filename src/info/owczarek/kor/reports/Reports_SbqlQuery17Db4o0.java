package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery17Db4o0 implements Db4oSBQLQuery<java.lang.Boolean> {
    private java.util.Collection<java.lang.Object> niceNames;

    public Reports_SbqlQuery17Db4o0(
        java.util.Collection<java.lang.Object> niceNames) {
        this.niceNames = niceNames;
    }

    /**
     * query='db.( any Customer.getFirstName() as name name in niceNames)'
    '
     **/
    public java.lang.Boolean executeQuery(final ObjectContainerBase ocb,
        final Transaction t) {
        //evaluateExpression - start  any Customer.getFirstName() as name name in niceNames
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitForanyExpression - start  any Customer.getFirstName() as name name in niceNames
        //visitAsExpression - start Customer.getFirstName() as name
        //visitDotExpression - start Customer.getFirstName()
        //visitIdentifierExpression - start Customer
        final java.util.Collection<info.owczarek.kor.model.Customer> _ident_Customer =
            new java.util.ArrayList<info.owczarek.kor.model.Customer>();
        ClassMetadata _classMeta19 = ocb.classCollection()
                                        .getClassMetadata("info.owczarek.kor.model.Customer");
        long[] _ids19 = _classMeta19.getIDs(transLocal);

        for (long _id19 : _ids19) {
            LazyObjectReference _ref19 = transLocal.lazyReferenceFor((int) _id19);
            _ident_Customer.add((info.owczarek.kor.model.Customer) _ref19.getObject());
        }

        //visitIdentifierExpression - end Customer
        java.util.Collection<java.lang.String> _dotResult = new java.util.ArrayList<java.lang.String>();
        int _dotIndex = 0;

        for (info.owczarek.kor.model.Customer _dotEl : _ident_Customer) {
            if (_dotEl == null) {
                continue;
            }

            if (_dotEl != null) {
                ocb.activate(_dotEl, 1);
            }

            //visitMethodExpression - start getFirstName()
            java.lang.String _mth_getFirstNameResult = _dotEl.getFirstName();

            if (_mth_getFirstNameResult != null) {
                ocb.activate(_mth_getFirstNameResult, 1);
            }

            //visitMethodExpression - end getFirstName()
            if (_mth_getFirstNameResult != null) {
                ocb.activate(_mth_getFirstNameResult, 1);
            }

            _dotResult.add(_mth_getFirstNameResult);
            _dotIndex++;
        }

        //visitDotExpression - end Customer.getFirstName()
        java.util.Collection<java.lang.String> _asResult_name = _dotResult;

        //visitAsExpression - end Customer.getFirstName() as name
        java.lang.Boolean _anyResult = false;
        Integer _anyIndex = 0;

        for (java.lang.String _anyEl : _asResult_name) {
            if (_anyEl != null) {
                ocb.activate(_anyEl, 1);
            }

            //visitBinaryAExpression - start name in niceNames
            //visitIdentifierExpression - start name
            java.lang.String _ident_name = _anyEl;

            if (_ident_name != null) {
                ocb.activate(_ident_name, 1);
            }

            //visitIdentifierExpression - end name
            //visitIdentifierExpression - start niceNames
            java.util.Collection<java.lang.Object> _ident_niceNames = niceNames;

            //visitIdentifierExpression - end niceNames
            //OperatorIn - start name in niceNames
            Collection _inLeftCol0 = new ArrayList();
            _inLeftCol0.add(_ident_name);

            Collection _inRightCol0 = new ArrayList(_ident_niceNames);
            java.lang.Boolean _inResult = _inRightCol0.containsAll(_inLeftCol0);

            //OperatorIn - end name in niceNames
            //visitBinaryAExpression - end name in niceNames
            if (_inResult) {
                _anyResult = true;

                break;
            }
        }

        //visitForanyExpression - end  any Customer.getFirstName() as name name in niceNames
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_anyResult, ocb);

        return _anyResult;

        //evaluateExpression - end  any Customer.getFirstName() as name name in niceNames
    }
}
