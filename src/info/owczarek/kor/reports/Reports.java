package info.owczarek.kor.reports;

import com.db4o.*;
import java.io.*;
import info.owczarek.kor.model.*;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service()
public class Reports {
    
    public Reports() {
        super();
    }
    @Autowired()
    private ObjectContainer db;
    public static final String DB_FILENAME = "shop_database.db";
    
    public ObjectContainer getConnection() {
        if (db == null) {
            File dbFile = new File(DB_FILENAME);
            db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), DB_FILENAME);
        }
        return db;
    }
    
    public static void main(String[] args) {
        Reports test = new Reports();
        test.getConnection();
    }
    
    public Object getComplexQueryResult(String lastName) {
        Object result = new Reports_SbqlQuery0(db,lastName).executeQuery();
        return result;
    }
    
    public Object getCustomerOrdersBetween(String customerLogin, Date creationDateAfter, Date creationDateBefore) {
        Object result = new Reports_SbqlQuery1(db,customerLogin,creationDateAfter,creationDateBefore).executeQuery();
        return result;
    }
    
    public Object getCustomersWithNumberOfOrders() {
        Object result = new Reports_SbqlQuery2(db).executeQuery();
        return result;
    }
    
    public Object getOrdersBetween(Date from, Date to) {
        Object result = new Reports_SbqlQuery3(db,from,to).executeQuery();
        return result;
    }
    
    public double getYearSalary(String employeeId) {
        Collection<Double> result = new Reports_SbqlQuery4(db,employeeId).executeQuery();
        return (Double)result.iterator().next();
    }
    
    public double getGrossSalary(String employeeId, double tax) {
        Collection<Double> result = new Reports_SbqlQuery5(tax,db,employeeId).executeQuery();
        return (Double)result.iterator().next();
    }
    
    public Employee getEmployee(String employeeId) {
        Collection result = new Reports_SbqlQuery6(db,employeeId).executeQuery();
        return (Employee)result.iterator().next();
    }
    
    public Double getAverageSalary() {
        Object result = new Reports_SbqlQuery7(db).executeQuery();
        return (Double)result;
    }
    
    public Double getMinSalary() {
        Object result = new Reports_SbqlQuery8(db).executeQuery();
        return (Double)result;
    }
    
    public Double getMaxSalary() {
        Object result = new Reports_SbqlQuery9(db).executeQuery();
        return (Double)result;
    }
    
    public int getNumberOfEmployees() {
        Object result = new Reports_SbqlQuery10(db).executeQuery();
        return (Integer)result;
    }
    
    public Double getSalaryOutgoings() {
        Object result = new Reports_SbqlQuery11(db).executeQuery();
        return (Double)result;
    }
    
    public Collection<Double> getMonthsToGetLamborghini(double salary, double interestRate, double priceOfLamborghini) {
        Object result = new Reports_SbqlQuery12(db,salary,interestRate,priceOfLamborghini).executeQuery();
        return (Collection<Double>)result;
    }
    
    public Object getAllCategories() {
        Object result = new Reports_SbqlQuery13(db).executeQuery();
        return result;
    }
    
    public List<Object> getEmployees() {
        Object result = new Reports_SbqlQuery14(db).executeQuery();
        return (List<Object>)result;
    }
    
    public Object getRichEmployees() {
        double richnessLevel = 10000.0;
        Object result = new Reports_SbqlQuery15(db,richnessLevel).executeQuery();
        return result;
    }
    
    public boolean checkMinimumWage() {
        double minimumWage = 1680.0;
        return checkMinimumWage(minimumWage);
    }
    
    public boolean checkMinimumWage(double minimumWage) {
        Object result = new Reports_SbqlQuery16(db,minimumWage).executeQuery();
        return (Boolean)result;
    }
    
    public boolean hasAnyCustomerNiceName() {
        Set niceNames = new HashSet();
        niceNames.add("Marta");
        niceNames.add("Marika");
        niceNames.add("Dawid");
        boolean result = hasAnyCustomerName(niceNames);
        return result;
    }
    
    public boolean hasAnyCustomerName(Set niceNames) {
        Object result = new Reports_SbqlQuery17(db,niceNames).executeQuery();
        return (Boolean)result;
    }
    
    public boolean isThereAMillionaireAmongEmployees() {
        Object result = new Reports_SbqlQuery18(db).executeQuery();
        return (Boolean)result;
    }
}