package info.owczarek.kor.reports;

import com.db4o.*;

import com.db4o.foundation.*;

import com.db4o.internal.*;
import com.db4o.internal.btree.*;

import info.owczarek.kor.model.*;

import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import pl.wcislo.sbql4j.db4o.*;
import pl.wcislo.sbql4j.exception.*;
import pl.wcislo.sbql4j.java.model.runtime.*;
import pl.wcislo.sbql4j.java.model.runtime.factory.*;
import pl.wcislo.sbql4j.java.utils.ArrayUtils;
import pl.wcislo.sbql4j.java.utils.OperatorUtils;
import pl.wcislo.sbql4j.java.utils.Pair;
import pl.wcislo.sbql4j.lang.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.codegen.simple.*;
import pl.wcislo.sbql4j.lang.db4o.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.interpreter.*;
import pl.wcislo.sbql4j.lang.db4o.codegen.nostacks.*;
import pl.wcislo.sbql4j.lang.parser.expression.*;
import pl.wcislo.sbql4j.lang.parser.expression.OrderByParamExpression.SortType;
import pl.wcislo.sbql4j.lang.parser.terminals.*;
import pl.wcislo.sbql4j.lang.parser.terminals.operators.*;
import pl.wcislo.sbql4j.lang.types.*;
import pl.wcislo.sbql4j.lang.xml.*;
import pl.wcislo.sbql4j.model.*;
import pl.wcislo.sbql4j.model.collections.*;
import pl.wcislo.sbql4j.util.*;
import pl.wcislo.sbql4j.util.Utils;
import pl.wcislo.sbql4j.xml.model.*;
import pl.wcislo.sbql4j.xml.parser.store.*;

import java.io.*;

import java.util.*;


public class Reports_SbqlQuery3Db4o0 implements Db4oSBQLQuery<java.util.Collection<info.owczarek.kor.model.Order>> {
    private java.util.Date from;
    private java.util.Date to;

    public Reports_SbqlQuery3Db4o0(java.util.Date from, java.util.Date to) {
        this.from = from;
        this.to = to;
    }

    /**
     * query='db.(Order as o where o.getCreationDate() >= from and o.getCreationDate() <= to)'
    '
     **/
    public java.util.Collection<info.owczarek.kor.model.Order> executeQuery(
        final ObjectContainerBase ocb, final Transaction t) {
        //evaluateExpression - start Order as o where o.getCreationDate() >= from and o.getCreationDate() <= to
        final LocalTransaction transLocal = (LocalTransaction) t;

        //visitWhereExpression - start Order as o where o.getCreationDate() >= from and o.getCreationDate() <= to
        //visitAsExpression - start Order as o
        //visitIdentifierExpression - start Order
        final java.util.Collection<info.owczarek.kor.model.Order> _ident_Order = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        ClassMetadata _classMeta6 = ocb.classCollection()
                                       .getClassMetadata("info.owczarek.kor.model.Order");
        long[] _ids6 = _classMeta6.getIDs(transLocal);

        for (long _id6 : _ids6) {
            LazyObjectReference _ref6 = transLocal.lazyReferenceFor((int) _id6);
            _ident_Order.add((info.owczarek.kor.model.Order) _ref6.getObject());
        }

        //visitIdentifierExpression - end Order
        java.util.Collection<info.owczarek.kor.model.Order> _asResult_o = _ident_Order;

        //visitAsExpression - end Order as o
        java.util.Collection<info.owczarek.kor.model.Order> _whereResult = new java.util.ArrayList<info.owczarek.kor.model.Order>();
        int _whereLoopIndex = 0;

        for (info.owczarek.kor.model.Order _whereEl : _asResult_o) {
            if (_whereEl == null) {
                continue;
            }

            if (_whereEl != null) {
                ocb.activate(_whereEl, 1);
            }

            //visitBinaryAExpression - start o.getCreationDate() >= from and o.getCreationDate() <= to
            //OperatorAnd - start o.getCreationDate() >= from and o.getCreationDate() <= to
            //visitBinaryAExpression - start o.getCreationDate() >= from
            //visitDotExpression - start o.getCreationDate()
            //visitIdentifierExpression - start o
            info.owczarek.kor.model.Order _ident_o = _whereEl;

            if (_ident_o != null) {
                ocb.activate(_ident_o, 1);
            }

            //visitIdentifierExpression - end o
            info.owczarek.kor.model.Order _dotEl = _ident_o;

            if (_ident_o != null) {
                ocb.activate(_ident_o, 2);
            }

            //visitMethodExpression - start getCreationDate()
            java.util.Date _mth_getCreationDateResult = _dotEl.getCreationDate();

            if (_mth_getCreationDateResult != null) {
                ocb.activate(_mth_getCreationDateResult, 1);
            }

            //visitMethodExpression - end getCreationDate()
            //visitDotExpression - end o.getCreationDate()
            //visitIdentifierExpression - start from
            java.util.Date _ident_from = from;

            //visitIdentifierExpression - end from
            //OperatorMoreOrEqual - start o.getCreationDate() >= from
            Boolean _more_or_equalResult = _mth_getCreationDateResult.compareTo(_ident_from) >= 0;

            //OperatorMoreOrEqual - end o.getCreationDate() >= from
            //visitBinaryAExpression - end o.getCreationDate() >= from
            java.lang.Boolean _andResult;

            if (!_more_or_equalResult) {
                _andResult = false;
            } else {
                //visitBinaryAExpression - start o.getCreationDate() <= to
                //visitDotExpression - start o.getCreationDate()
                //visitIdentifierExpression - start o
                info.owczarek.kor.model.Order _ident_o1 = _whereEl;

                if (_ident_o1 != null) {
                    ocb.activate(_ident_o1, 1);
                }

                //visitIdentifierExpression - end o
                info.owczarek.kor.model.Order _dotEl1 = _ident_o1;

                if (_ident_o1 != null) {
                    ocb.activate(_ident_o1, 2);
                }

                //visitMethodExpression - start getCreationDate()
                java.util.Date _mth_getCreationDateResult1 = _dotEl1.getCreationDate();

                if (_mth_getCreationDateResult1 != null) {
                    ocb.activate(_mth_getCreationDateResult1, 1);
                }

                //visitMethodExpression - end getCreationDate()
                //visitDotExpression - end o.getCreationDate()
                //visitIdentifierExpression - start to
                java.util.Date _ident_to = to;

                //visitIdentifierExpression - end to
                //OperatorLessOrEqual - start o.getCreationDate() <= to
                Boolean _less_or_equalResult = _mth_getCreationDateResult1.compareTo(_ident_to) <= 0;
                //OperatorLessOrEqual - end o.getCreationDate() <= to
                //visitBinaryAExpression - end o.getCreationDate() <= to
                _andResult = _less_or_equalResult;
            }

            //OperatorAnd - end o.getCreationDate() >= from and o.getCreationDate() <= to
            //visitBinaryAExpression - end o.getCreationDate() >= from and o.getCreationDate() <= to
            if (_andResult) {
                _whereResult.add(_whereEl);
            }

            _whereLoopIndex++;
        }

        //visitWhereExpression - end Order as o where o.getCreationDate() >= from and o.getCreationDate() <= to
        pl.wcislo.sbql4j.db4o.utils.DerefUtils.activateResult(_whereResult, ocb);

        return _whereResult;

        //evaluateExpression - end Order as o where o.getCreationDate() >= from and o.getCreationDate() <= to
    }
}
