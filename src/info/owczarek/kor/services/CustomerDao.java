package info.owczarek.kor.services;

import info.owczarek.kor.model.Customer;
import info.owczarek.kor.model.Employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

@Service
public class CustomerDao {
	@Autowired
	private ObjectContainer db;

	public List<Customer> getAllCustomers() {
		return db.query(Customer.class);
	}
	
	public Customer getCustomer(String id) {
		if (id == null) {
			return null;
		}
		
		Customer customer = new Customer();
		customer.setId(id);
		ObjectSet<Customer> result = db.queryByExample(customer);
		if (result.hasNext()) {
			return result.next();
		} else {
			return null;
		}
	}

	public void addCustomer(Customer client) {
		db.store(client);
	}
	
	public void remove(String id) {
		Customer customer = new Customer();
		customer.setId(id);
		ObjectSet<Customer> result = db.queryByExample(customer);
		while (result.hasNext()) {
			Customer found = (Customer) result.next();
			db.delete(found);
		}
	}
}
