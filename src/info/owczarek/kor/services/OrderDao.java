package info.owczarek.kor.services;

import info.owczarek.kor.model.Order;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

@Service
public class OrderDao {
	@Autowired
	private ObjectContainer db;

	public List<Order> getAllOrders() {
		return db.query(Order.class);
	}

	public void addOrder(Order order) {
		db.store(order);
	}

	public void remove(String id) {
		Order order = new Order();
		order.setId(id);
		ObjectSet<Order> result = db.queryByExample(order);
		while (result.hasNext()) {
			Order found = (Order) result.next();
			db.delete(found);
		}
	}
}
