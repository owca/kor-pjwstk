package info.owczarek.kor.services;

import info.owczarek.kor.model.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

@Service
public class ProductDao {
	@Autowired
	private ObjectContainer db;

	public List<Product> getAllProducts() {
		return db.query(Product.class);
	}
	
	public Product getProduct(String id) {
		Product product = new Product();
		product.setId(id);
		ObjectSet<Product> result = db.queryByExample(product);
		if (result.hasNext()) {
			Product found = (Product) result.next();
			return found;
		} else {
			return null;
		}
	}

	public void addProduct(Product product) {
		db.store(product);
	}

	public void remove(String id) {
		Product product = new Product();
		product.setId(id);
		ObjectSet<Product> result = db.queryByExample(product);
		while (result.hasNext()) {
			Product found = (Product) result.next();
			db.delete(found);
		}
	}
}
