package info.owczarek.kor.services;

import info.owczarek.kor.model.Employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

@Service
public class EmployeeDao {
	@Autowired
	private ObjectContainer db;

	public List<Employee> getAllEmployees() {
		return db.query(Employee.class);
	}

	public void addEmployee(Employee employee) {
		db.store(employee);
	}

	public void remove(String id) {
		Employee employee = new Employee();
		employee.setId(id);
		ObjectSet<Employee> result = db.queryByExample(employee);
		while (result.hasNext()) {
			Employee found = (Employee) result.next();
			db.delete(found);
		}
	}

	public Employee getEmployee(String id) {
		if (id == null) {
			return null;
		}
		
		Employee employee = new Employee();
		employee.setId(id);
		ObjectSet<Employee> result = db.queryByExample(employee);
		if (result.hasNext()) {
			return result.next();
		} else {
			return null;
		}
	}
}
