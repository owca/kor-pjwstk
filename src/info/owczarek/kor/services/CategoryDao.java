package info.owczarek.kor.services;

import info.owczarek.kor.model.Category;
import info.owczarek.kor.model.Employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

@Service
public class CategoryDao {
	@Autowired
	private ObjectContainer db;

	public List<Category> getAllCategories() {
		return db.query(Category.class);
	}

	public Category getCategory(String name) {
		ObjectSet<Category> result = db.queryByExample(new Category(name));
		if (result.hasNext()) {
			Category found = (Category) result.next();
			return found;
		} else {
			return null;
		}
	}

	public void addCategory(Category category) {
		db.store(category);
	}

	public void remove(String name) {
		ObjectSet<Category> result = db.queryByExample(new Category(name));
		while (result.hasNext()) {
			Category found = (Category) result.next();
			db.delete(found);
		}
	}
}
